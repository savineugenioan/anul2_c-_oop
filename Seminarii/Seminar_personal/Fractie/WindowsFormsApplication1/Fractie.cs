﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class Fractie
    {
        public int x, y;
        public static Fractie inmultire(Fractie f1, Fractie f2)
        {
            Fractie f3 = new Fractie();
            f3.x = f1.x * f2.x;
            f3.y = f1.y * f2.y;
            return f3;
        }
        public static Fractie Creeaza(int x, int y)
        {
            Fractie f = new Fractie();
            f.x = x;
            f.y = y;
            if (y != 0)
                f.y = y;
            else
            {
                MessageBox.Show("Numitorul nu poate fi 0 !");
                f.x = 0;f.y = 1;
            }
            return f;  
        }
        public static string Serializare(Fractie f)
        {
            if (f.y == 1)
                return f.x.ToString();
            else
                return f.x + "/" + f.y;
        }
    }
}
