﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Cerc : FGeom
    {
        int raza;
        public Cerc(int x, int y, int raza) : base(x, y)
        {

            this.raza = raza;
        }
        public override double Arie()
        {
            return 3.1415 * raza * raza;
        }
        public override double Perimetru()
        {
            return 3.1415 * 2 * raza;
        }
    }
}
