﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<FGeom> figuri = new List<FGeom>();
            figuri.Add(new Dreptunghi(2,3,4,5));
            figuri.Add(new Cerc(1,2,5));
            figuri.Add(new Dreptunghi(2, 3, 4, 6));
            figuri.Add(new Cerc(1, 2, 2));
            figuri.Add(new Dreptunghi(2, 3, 7, 7));
            figuri.Add(new Cerc(1, 2, 7));
            figuri.Sort(FGeom.Compare);
            foreach (var a in figuri)
                Console.WriteLine("Figura '{0}' are aria egala cu {1} si perimetrul egal cu {2} ",a.GetType().Name, a.Arie(),a.Perimetru());
            Console.ReadKey();
        }
    }
}
