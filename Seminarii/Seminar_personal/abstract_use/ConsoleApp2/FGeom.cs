﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    abstract class  FGeom
    {
        protected int x, y;
        public FGeom(int x,int y)
        {
            this.x = x;
            this.y = y;
        }
        public abstract double Arie();
        public abstract double Perimetru();
        public static int Compare(FGeom f1,FGeom f2)
        {
            return f1.Arie().CompareTo(f2.Arie());
        }
    }
}
