﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dreptunghi : FGeom
    {
        int L, l;
        public Dreptunghi(int x, int y, int L, int l) : base(x, y)
        {

            this.L = L;
            this.l = l;
        }
        public override double Arie()
        {
            return L * l;
        }
        public override double Perimetru()
        {
            return 2 * l + 2 * L;
        }
    }
}
