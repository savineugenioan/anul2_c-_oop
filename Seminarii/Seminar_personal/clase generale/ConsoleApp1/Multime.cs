﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Multime<T> : List<T> where T : IComparable
    {
        public static Multime<T> operator +(Multime<T> x,T y)
        { 
            Multime<T> m = new Multime<T>();
            m.AddRange(x);
            m.Add(y);
            m.Sort();
            return m;
        }
        public override string ToString()
        {
            string str="";
            foreach (T i in this)
                str += i + Environment.NewLine;
            return str;
        }
    }
}
