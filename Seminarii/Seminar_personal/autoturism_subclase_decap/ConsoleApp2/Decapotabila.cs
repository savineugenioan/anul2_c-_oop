﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Decapotabila : Autoturism
    {
        string tip;//hard sau soft
        public Decapotabila(string marca,double pret,int an,string tip) : base(marca, pret, an)
        {
            this.tip = tip;
        }
        public Decapotabila(string linie) : base(linie)
        {
            string[] atomi = linie.Split();
            tip = atomi[3];
        }
        public override string ToString()
        {
            return base.ToString()+" de tipul "+ tip;
        }
    }
}
