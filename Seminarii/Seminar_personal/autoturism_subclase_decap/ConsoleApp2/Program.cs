﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Autoturism> Parc = new List<Autoturism>();
            string path="masini.txt";
            StreamReader file = new StreamReader(path);
            while (!file.EndOfStream)
            {
                string rand = file.ReadLine();
                string rand2 = rand.Remove(0, 2);   
                if (rand[0] == '1')
                    Parc.Add(new Autoturism(rand2));
                else 
                    Parc.Add(new Decapotabila(rand2));
            }
            //Parc.Add(new Decapotabila("aa", 123.4, 1987, "soft"));
            //Parc.Add(new Decapotabila("aa", 123.4, 1987, "soft"));
            Autoturism.Afisare(Parc);
            Parc.Sort(Autoturism.Compare);
            Console.WriteLine();
            Autoturism.Afisare(Parc);
            file.Close();
            Autoturism ab = new Autoturism("MM 4.5 1876");

            Console.ReadKey();
        }
    }
}

