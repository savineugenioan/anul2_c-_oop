﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Student
    {
        string nume; // se numeste camp
        int nota;
        public int Nota // se numeste proprietate
        {
          get { return nota; } // pentru Console.WriteLine(a.Nota);
            set // pentru a.Nota = -9;
            {
                if (value > 0 && value <= 10)
                    nota = value;
            }
        }
        public string Nume
        {
            get { return nume; }
            //set { nume = value; }
        }
        public Student(string nume, int nota)
        {
            this.nume = nume;
            this.nota = nota;
        }
        public void Afis() // se numeste metoda 
        {
            Console.WriteLine("Studentul {0} are nota {1}", this.nume, this.nota);
        }
        public int Marire_nota(int bonus)
        {
            return nota += bonus;
        }
        public bool Este_restantier()
        {
            return nota < 5;
        }
        public static int Compare(Student a, Student b)
        {
            return a.nume.CompareTo(b.nume);
        }
        public int Compare_to(Student b)
        {
            return this.nume.CompareTo(b.nume);
        }
    }
}
