﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Fractie
    {
        int a, b;
        public Fractie(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        public Fractie(string x)
        {
            string[] atomi = new string[2];
            atomi = x.Split('/');
            this.a = int.Parse(atomi[0]);
            this.b = int.Parse(atomi[1]);
        }
        public override string ToString()
        {
            return a+"/"+b;
        }
        public int CompareTo(Fractie f)
        {
            return ((double)this).CompareTo((double)f);
        }
        public static int Compare(Fractie f1,Fractie f2)
        {
            return ((double)f1).CompareTo((double)f2);
        }
        static Fractie ireductibil(Fractie f)
        {
            int nr = f.a, num = f.b;
            while (nr != num)
                if (nr > num)
                    nr -= num;
                else
                    num -= nr;

            return new Fractie(f.a/nr,f.b/nr)   ;
        }
        public static Fractie operator *(Fractie a,Fractie b)
        {
            Fractie x = new Fractie(a.a * b.a, a.b * b.b);
            return Fractie.ireductibil(x);
        }
        public static Fractie operator -(Fractie a)
        {
            return new Fractie(-a.a,a.b);
        }
        public static bool operator >(Fractie a, Fractie b)
        {
            return a.a/a.b>b.a/b.b;
        }
        public static bool operator <(Fractie a, Fractie b)
        {
            return a.a / a.b < b.a / b.b;
        }
        public static explicit operator double(Fractie a)
        {
            return (double)a.a / a.b;
        }
    }
}
