﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Fractie a = new Fractie(1, 4);
            Fractie b = new Fractie(3, 5);
            Console.WriteLine("Primul numar = {0:N}",(double)a);// afiseaza doar 2 zecimale
            //Console.WriteLine(a.CompareTo(b));
            List<Fractie> Lista = new List<Fractie>();
            StreamReader f = new StreamReader("fisier.txt");
            while(!f.EndOfStream)
            {
                Lista.Add(new Fractie(f.ReadLine()));
            }f.Close();
            Lista.Sort(Fractie.Compare);
            foreach(Fractie x in Lista)
            {
                Console.WriteLine(x);
            }
            Console.ReadKey();
        }
    }
}
