﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        string nume;
        int grupa;
        double nota;
        public string Nume
            {
                get { return this.nume; }
                set { this.nume = value; }
            }
        public double Nota
        {
            get { return this.nota; }
            set
            {
                if(value>0 && value <11)
                this.nota = value;
            }
        }
        public Student(string nume,int grupa,double nota)
        {
            contor++;
            this.nume = nume;
            this.grupa = grupa;
            this.nota = nota;
            id = contor;
        }
        public Student()
        {
            this.nume = "anonim";
            contor++;
            id = contor;
        }
        public override string ToString()
        {
            return "Studentul "+nume+" este in grupa "+grupa+" si are nota "+nota+" id: "+id;
        }
        public Student(string rand)
        {
            string[] atomi = rand.Split(',');
            this.nume = atomi[0];
            this.grupa = int.Parse(atomi[1]);
            this.nota = double.Parse(atomi[2]);
            contor++;
            id = contor;
        }
        public static int contor = 0;
        readonly int id;
        
    }
}
