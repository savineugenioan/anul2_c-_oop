﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s1 = new Student("Ion", 614,10);
            Console.WriteLine(s1);
            //Console.WriteLine(Student.contor);
            //Student s2 = new Student(Console.ReadLine());
            //Console.WriteLine(s2);
            //Console.WriteLine(Student.contor);
            Student s3 = new Student("ana,123,5");
            Console.WriteLine(s3);
            //Console.WriteLine(Student.contor);
            Console.WriteLine();
            List<Student> studenti = new List<Student>();
            studenti.Add(s1);
            studenti.Add(s3);
            studenti.Add(new Student("gigi,615,8"));
            studenti.Add(new Student("ama,615,8"));
            foreach(Student s in studenti)
            {
                Console.WriteLine(s);
            }
            double m = 0;
            foreach(Student s in studenti)
            {
                m += s.Nota;   
            }
            m = m / Student.contor;
            Console.WriteLine("Media este {0}", m);
            Console.ReadKey();
        }
    }
}
