﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Aplicatie
{
    class Program
    {
        static void Main(string[] args)
        {

            Student s1 = new Student("Maria", 619, 10);
            Student s2 = new Student("Anghel", 610, 7);
            s1.Nota = -5;
            Console.WriteLine(s1);
            List<Student> student = new List<Student>();
            student.Add(new Student("Ion,615,9"));
            student.Add(s1);
            student.Add(new Student("Anda,614,7"));
            foreach (Student s in student)
                Console.WriteLine(s);
            string[] randuri = File.ReadAllLines("fisier.txt");
            foreach (string rand in randuri)
            {
                student.Add(new Student(rand));
            }
            foreach (Student stud in student)
            {
                Console.WriteLine(stud);
            }
            Student maxStudent = s1;
            foreach (Student stud in student)
            {
                if (maxStudent.Nota < stud.Nota)
                    maxStudent = stud;
            }
            Console.WriteLine(maxStudent);

            Console.ReadKey();
        }
    }
}
