﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie
{
    class Student
    {
        int nota;
        int grupa;
        string nume;

        public Student(string nume, int grupa, int nota)
        {
            this.nota = nota;
            this.grupa = grupa;
            this.nume = nume;
        }
        public Student(string infoStudent)
        {
            string[] informatii = infoStudent.Split(',');
            nume = informatii[0];
            grupa = int.Parse(informatii[1]);
            nota = int.Parse(informatii[2]);
        }
        public int Nota
        {
            get { return nota; }
            set
            {
                if (value > 0)
                    nota = value;       
            }
        }
        public override string ToString()
        {
            return nume + "," + grupa + "," + nota;
        }
        
    }
}
