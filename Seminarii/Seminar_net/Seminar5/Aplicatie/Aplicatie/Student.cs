﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie
{
    class Student
    {
        protected int nota;
        protected int grupa;
        protected string nume;

        public Student(string nume, int grupa, int nota)
        {
            this.nota = nota;
            this.grupa = grupa;
            this.nume = nume;
        }
        public Student(string infoStudent)
        {
            string[] informatii = infoStudent.Split(',');
            nume = informatii[0];
            grupa = int.Parse(informatii[1]);
            nota = int.Parse(informatii[2]);
        }
        public int Nota
        {
            get { return nota; }
            set
            {
                if (value > 0)
                    nota = value;       
            }
        }
        public override string ToString()
        {
            return nume + "," + grupa + "," + nota;
        }
        public static Student operator +(Student s, int bonus)
        {

            return new Student(s.nume, s.grupa, s.nota + bonus);

        }
        public static bool operator ==(Student s1, int grupa)
        {
            return (s1.grupa == grupa);
        }
        public static bool operator !=(Student s1, int grupa)
        {
            return !(s1.grupa == grupa);
        }
        public static int Compare(Student s1, Student s2)
        {
            return -(s1.nota.CompareTo(s2.nota));
        }
    }
}
