﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie
{
    class Bursier:Student
    {
        int bursa;
        public Bursier(string nume, int grupa, int nota, int bursa)
        :base(nume,grupa,nota)
        {
            this.bursa = bursa;
        }


        public override string ToString()
        {
            return base.ToString() +"," + bursa;
        }
    }
}
