﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6
{
    class Fractie
    {
        public int x, y;
        public static Fractie inmultire(Fractie f1, Fractie f2)
        {
            return new Fractie(f1.x * f2.x, f1.y * f2.y);  
        }
        public Fractie(int x, int y)
        {
            this.x = x; 
            this.y = y;

        }
        public string toString()
        { return x + "/" + y;
        }
    }
}
