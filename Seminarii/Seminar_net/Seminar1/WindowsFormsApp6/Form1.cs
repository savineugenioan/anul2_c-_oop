﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           Fractie f1 = new Fractie(int.Parse(textBox1.Text), int.Parse(textBox2.Text));
           Fractie f2 = new Fractie(int.Parse(textBox3.Text), int.Parse(textBox4.Text));
           Fractie f3 = Fractie.inmultire(f1, f2);
            textBox5.Text = f3.toString();
        }
    }
}
