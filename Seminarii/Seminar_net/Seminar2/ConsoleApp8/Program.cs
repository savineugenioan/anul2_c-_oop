﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Student S1 = new Student("Ion", 9);
            Student S2 = new Student("Vasile", 5);
            S1.Afisare();
            S2.MarireNota(2);
            S2.Afisare();
            if (S1.CompareTo(S2) == 0) Console.WriteLine("Note egale");
            else Console.WriteLine("Note dif");
            if(Student.CompareTo(S1,S2)==0) Console.WriteLine("Note egale");
            else Console.WriteLine("Note dif");
            if (S2.EstePromovat()) Console.WriteLine("EstePromovat");
            Console.WriteLine("Media " + (S1.Nota + S2.Nota) / 2);
            S1.Nota = 10;
            S1.Afisare();
            Console.ReadKey();
        }
   
    }
   
}
