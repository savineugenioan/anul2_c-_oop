﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class Student
    {
        string nume;
        int nota;
        public Student (string nume, int nota)
        {
            this.nume = nume;
            this.nota = nota;
            
        }
        public int Nota //proprietate
        {
            get { return nota; }
            set { if (value > 0 && value <= 10) nota = value;} 
        }
    public void Afisare()
        {
            Console.WriteLine("Studentul " + nume + " are nota " + nota);
        }
    public int MarireNota(int bonus)
        {
            return nota += bonus;
        }
    public bool EstePromovat()
        {
            if (nota > 5) return true;
                else return false;
        }
     public int CompareTo(Student S2)
        {
            return this.nota.CompareTo(S2.Nota);
        }
    public static int CompareTo(Student S1, Student S2)
        {
            return S1.nota.CompareTo(S2.Nota);
        }
    }
}
