﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Student S1 = new Student("Maria", 619, 10);
            //S1.Nota = -5;
            List <Student> Student= new List <Student>();
            Student.Add(new Student("Ion, 615, 9"));
            Student.Add(S1);
            foreach (Student s in Student)
                Console.WriteLine(s);
            string[] randuri = File.ReadAllLines("fis.txt");
            foreach (string rand in randuri) Student.Add(new Student(rand));
            foreach (Student s in Student) Console.WriteLine(s);
            Console.ReadKey();
        
        }
    }
}
