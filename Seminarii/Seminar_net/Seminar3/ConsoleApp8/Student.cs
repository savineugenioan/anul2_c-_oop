﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class Student
    {
        string nume;
        double nota;
        int grupa;

        public Student (string nume, int grupa, int nota)
        {
            this.nume = nume;
            this.nota = nota;
            this.grupa = grupa;

        }

        public Student (string rand)
        {
            string[] atomi = rand.Split(',');
            nume = atomi[0];
            grupa = int.Parse(atomi[1]);
            nota = double.Parse(atomi[2]);
        }
        public override string ToString()
        {
            return nume + "," + grupa + "," + nota;

        }
        public void Afisare()
        {
            Console.WriteLine("Studentul " + nume + " din grupa " + grupa + " are nota " + nota);
            
        }
    }
}
