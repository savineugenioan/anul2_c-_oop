﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class Produs
    {
        string den;
        List<Ingredient> ingrediente;
        public Produs(string den)
        {
            this.den = den;
            ingrediente = new List<Ingredient>();  
        }
        public static Produs operator + (Produs p, Ingredient i)
        {
            Produs q = new Produs(p.den);
            q.ingrediente = p.ingrediente;
            q.ingrediente.Add(i);
            return q;
        }
        public override string ToString()
        {
            string s = "Produsul "+ den + " contine ingredientele "+ Environment.NewLine;
            foreach (Ingredient i in ingrediente) s += i.ToString() + Environment.NewLine;
            return s;

        } 
        public double Pret()
        {
            double p = 0;
            foreach (Ingredient i in ingrediente) p += i.Pret();
            return p;

        }
    }
}
