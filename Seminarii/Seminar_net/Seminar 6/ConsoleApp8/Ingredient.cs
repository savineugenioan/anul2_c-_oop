﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class Ingredient
    {
        string den;
        int cantitate;
        double pretunitar;
        public Ingredient(string den, int cantitate, double pretunitar)
        {
            this.den = den;
            this.cantitate = cantitate;
            this.pretunitar = pretunitar;
        }
        public override string ToString()
        {
            return den + ',' + cantitate + ',' + pretunitar;
        }
        public double Pret()
        {
            return cantitate * pretunitar;

        }

    }
    
}
