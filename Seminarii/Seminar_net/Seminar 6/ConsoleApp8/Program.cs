﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Produs pizza = new Produs("Margherita");
            Ingredient a1 = new Ingredient("cascaval", 100, 0.5);
            Ingredient a2 = new Ingredient("sos rosii", 200, 0.25);
            Console.WriteLine(a1);
            Console.WriteLine(a2);
            pizza += a1;
            pizza += a2;
            Console.WriteLine(pizza + "si are pretul" + pizza.Pret());
            Console.ReadKey();

        }
    }
}
