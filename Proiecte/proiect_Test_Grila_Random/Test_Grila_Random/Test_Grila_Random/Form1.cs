﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
//using System.IO;

namespace Test_Grila_Random
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        static int corecte=0,nr_intrebare=0;
        List<Intrebari> Alese = new List<Intrebari>();
        string[] raspunsuri = new string[10];



        private void Form1_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            
            corecte = 0;
            List<Intrebari> intrebari = new List<Intrebari>();
            string connetionString = null;
            OleDbConnection cnn;
            OleDbCommand cmd;
            string sql = null;
            OleDbDataReader reader;
            string source = AppDomain.CurrentDomain.BaseDirectory;
            //connetionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\EUGEN-IOANSAVIN\Desktop\Test_Grila_Random\Test_Grila_Random\bin\Debug\grile.accdb";
            connetionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+source+ "grile.accdb";
            sql = "Select * From Grile";

            cnn = new OleDbConnection(connetionString);
            try
            {
                cnn.Open();
                cmd = new OleDbCommand(sql, cnn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    intrebari.Add(new Intrebari(reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString()));
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Random rnd = new Random();
            for(int x=0;x<10;x++)
            {
                int i = rnd.Next(0, intrebari.Count);
                Intrebari qstn= intrebari[i];
                string a, b, c,corect;
                a = qstn.a;
                b = qstn.b;
                c = qstn.c;
                corect = qstn.corect;
                i = rnd.Next(1, 4);
                switch(i)
                {
                    case 1: { if (qstn.corect == "a") qstn.corect = "b"; else if (qstn.corect == "b") qstn.corect = "a";string interm = qstn.a;qstn.a = qstn.b;qstn.b = interm; };break;
                    case 2: { if (qstn.corect == "a") qstn.corect = "c"; else if (qstn.corect == "c") qstn.corect = "a"; string interm = qstn.a; qstn.a = qstn.c; qstn.c = interm; } break;
                    case 3: { if (qstn.corect == "c") qstn.corect = "b"; else if (qstn.corect == "b") qstn.corect = "c"; string interm = qstn.c; qstn.c = qstn.b; qstn.b = interm; } break;
                }
                Alese.Add(qstn);
                intrebari.Remove(qstn);
            }
            AfisareIntrebare();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            for (int i = 0; i < 10; i++)
                if (raspunsuri[i] == Alese[i].corect)
                    corecte++;
            label4.Visible = true;
            button3.Enabled = false;
            label4.Text = "Rezultat : " + corecte + "/10";
        }

        private void button2_Click(object sender, EventArgs e)
        {

            nr_intrebare++;
            if (nr_intrebare == 9)
                button2.Enabled = false;
            button1.Enabled = true;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            if (raspunsuri[nr_intrebare] == "a")
                radioButton1.Checked = true;
            else if (raspunsuri[nr_intrebare] == "b")
                radioButton2.Checked = true;
            else if (raspunsuri[nr_intrebare] == "c")
                radioButton3.Checked = true;
            label2.Text = (nr_intrebare+1) + "";
            AfisareIntrebare();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nr_intrebare--;
            if (nr_intrebare == 0)
                button1.Enabled = false;
            button2.Enabled = true;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            if (raspunsuri[nr_intrebare] == "a")
                radioButton1.Checked = true;
            else if (raspunsuri[nr_intrebare] == "b")
                radioButton2.Checked = true;
            else if (raspunsuri[nr_intrebare] == "c")
                radioButton3.Checked = true;
            label2.Text = (nr_intrebare+1) + "";
            AfisareIntrebare();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
                raspunsuri[nr_intrebare] = "a";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
                raspunsuri[nr_intrebare] = "b";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
                raspunsuri[nr_intrebare] = "c";
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
        }
        private void radioButton2_Click_1(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
            radioButton1.Checked = false;
            radioButton3.Checked = false;
        }
        private void radioButton3_Click_1(object sender, EventArgs e)
        {
            radioButton3.Checked = true;
            radioButton2.Checked = false;
            radioButton1.Checked = false;
        }
        public void AfisareIntrebare()
        {
            label3.Text = Alese[nr_intrebare].intrebare;
            radioButton1.Text = Alese[nr_intrebare].a;
            radioButton2.Text = Alese[nr_intrebare].b;
            radioButton3.Text = Alese[nr_intrebare].c;
        }

    }
}
