﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace Gestiune_Masini
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string nume_fisier = "masini.txt";
        int nr_masina = 0;
        List<Autoturism> automobile = new List<Autoturism>();
    
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] x = File.ReadAllLines(nume_fisier);
            foreach(string y in x)
            {
                char[] chars = { ' ' };
                string[] atomi =y.Split(chars, StringSplitOptions.RemoveEmptyEntries);
                if (atomi.Length == 4)
                    automobile.Add(new Masina(y));
                else if(atomi.Length == 5)
                    automobile.Add(new Tir(y));
                AfisareMasina();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new Form_adaugare();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            nr_masina++;
            if (nr_masina == automobile.Count-1)
                button2.Enabled = false;
            button1.Enabled = true;
            label2.Text = (nr_masina + 1) + "";
            AfisareMasina();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nr_masina--;
            if (nr_masina == 0)
                button1.Enabled = false;
            button2.Enabled = true;
            label2.Text = (nr_masina+1) + "";
            AfisareMasina();
        }

        public void AfisareMasina()
        {
            if(automobile[nr_masina].GetType().Name == "Masina")
            {
                textBox1.Text = automobile[nr_masina].Marca;
                textBox2.Text = automobile[nr_masina].Pret;
                textBox3.Text = automobile[nr_masina].An;
                textBox4.Text = Masina.Tip((Masina)automobile[nr_masina]);
                textBox5.Visible = false;
                label6.Text = "                 Tip";
                label7.Visible = false;
            }
            else if (automobile[nr_masina].GetType().Name == "Tir")
            {
                textBox1.Text = automobile[nr_masina].Marca;
                textBox2.Text = automobile[nr_masina].Pret;
                textBox3.Text = automobile[nr_masina].An;
                textBox4.Text = Tir.Nr_Roti((Tir)automobile[nr_masina]);
                textBox5.Text = Tir.Diametru_Roti((Tir)automobile[nr_masina]);
                textBox5.Visible = true;
                label7.Visible = true;
                label6.Text = "Numar de roti";
            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
