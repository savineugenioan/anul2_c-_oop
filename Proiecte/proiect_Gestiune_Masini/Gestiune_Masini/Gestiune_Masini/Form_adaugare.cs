﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gestiune_Masini
{
    public partial class Form_adaugare : Form
    {
        string nume_fisier = "masini.txt";
        public Form_adaugare()
        {
            InitializeComponent();
        }

        private void Form_adaugare_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                groupBox1.Text = "Adaugare Masina";
                label6.Text = "                 Tip";
                label7.Visible = false;
                textBox5.Visible = false;
                textBox5.Text = "";
            }

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                groupBox1.Text = "Adaugare Tir";
                label6.Text = "Numar de roti";
                label7.Visible = true;
                textBox5.Visible = true;
            }

        }

        private void Form_adaugare_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form f = new Form1();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] chars = { ' ', '"', ',', '.', '/','?','!','\\','/','!'};
            if (verificare()==false)
            {
                MessageBox.Show("Va rugam sa completati toate campurile corect!");
                return;
            }

            File.AppendAllText(nume_fisier, textBox1.Text + " " + textBox2.Text + " " + textBox3.Text + " " + textBox4.Text + " " + textBox5.Text + Environment.NewLine);
            MessageBox.Show("Automobilul a fost adaugat cu success !");
            Form f = new Form1();
            this.Hide();
            f.Show();
            bool verificare()
            {
                double value;
                int valuare;
                if (textBox1.Text.Trim(chars) == string.Empty || textBox2.Text.Trim(chars) == string.Empty || textBox3.Text.Trim(chars) == string.Empty || textBox4.Text.Trim(chars) == string.Empty ||
                    !int.TryParse(textBox3.Text,out valuare) || !double.TryParse(textBox2.Text, out value))
                    return false;
                if (radioButton2.Checked == true && (textBox5.Text.Trim(chars) == string.Empty || !int.TryParse(textBox4.Text, out valuare) || !double.TryParse(textBox5.Text, out value)))
                    return false;
                return true;
            }
        }
    }
}
