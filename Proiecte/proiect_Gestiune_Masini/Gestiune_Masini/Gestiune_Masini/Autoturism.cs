﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Gestiune_Masini
{
    class Autoturism
    {
        string marca;
        double pret;
        int an;
        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }
        public string Pret
        {
            get { return pret.ToString(); }
            set { pret = double.Parse(value); }
        }
        public string An
        {
            get { return an.ToString(); }
            set { an = int.Parse(value); }
        }
        public Autoturism(string linie)
        {
            char[] chars = { ' ' };
            string[] atomi = linie.Split(chars, StringSplitOptions.RemoveEmptyEntries);
            marca = atomi[0];
            pret = double.Parse(atomi[1]);
            an = int.Parse(atomi[2]);
        }
        public Autoturism(string marca, double pret, int an)
        {
            this.marca = marca;
            this.pret = pret;
            this.an = an;
        }
        public void AddInFile(string nume_fisier)
        {
            File.AppendAllText(nume_fisier, this.ToString() + Environment.NewLine);
        }
        public static void Afisare(List<Autoturism> Parc)
        {
            foreach (Autoturism a in Parc)
                Console.WriteLine(a);
        }
        public override string ToString()
        {
            return marca + " " + pret + " " + an;
        }
        public static int Compare(Autoturism a, Autoturism b)
        {
            if (a.marca.CompareTo(b.marca) == 0)
                if (a.pret.CompareTo(b.pret) == 0)
                    return a.an.CompareTo(b.an);
                else
                    return a.pret.CompareTo(b.pret);
            return String.Compare(a.marca, b.marca);
        }
    }
}
