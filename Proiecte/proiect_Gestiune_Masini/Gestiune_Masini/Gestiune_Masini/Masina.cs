﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiune_Masini
{
    class Masina : Autoturism
    {
        string tip;
        public static string Tip(Masina x)
        {
            return x.tip;
        }
        public Masina(string marca, double pret, int an, string tip) : base(marca, pret, an)
        {
            this.tip = tip;
        }
        public Masina(string linie):base(linie)
        {
            char[] chars = { ' ' };
            string[] atomi = linie.Split(chars, StringSplitOptions.RemoveEmptyEntries);
            tip = atomi[3];
        }
    }
}
