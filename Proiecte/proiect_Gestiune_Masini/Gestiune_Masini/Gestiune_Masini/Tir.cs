﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiune_Masini
{
    class Tir : Autoturism
    {
        int nr_roti;
        double diametru_roata;
        public static string Nr_Roti(Tir x)
        {
            return x.nr_roti.ToString();
        }
        public static string Diametru_Roti(Tir x)
        {
            return x.diametru_roata.ToString();
        }
        public Tir(string marca, double pret, int an, string nr_roti,string diametru_roata) : base(marca, pret, an)
        {
            this.nr_roti = int.Parse(nr_roti);
            this.diametru_roata= double.Parse(diametru_roata);
        }
        public Tir(string linie) : base(linie)
        {
            char[] chars = { ' ' };
            string[] atomi = linie.Split(chars, StringSplitOptions.RemoveEmptyEntries);
            nr_roti = int.Parse(atomi[3]);
            diametru_roata = double.Parse(atomi[4]);
        }
    }
}
