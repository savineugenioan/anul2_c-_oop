﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class A_Comision : Angajat
    {
        protected double rata_comision,venit_produs;
        public A_Comision(string nume, string prenume, int ani_vechime,double rata_comision,double venit_produs) : base(nume, prenume, ani_vechime)
        {
            this.rata_comision = rata_comision;
            this.venit_produs = venit_produs;
            this.castiguri = this.Calcul_Castiguri();
        }
        public A_Comision(string linie) : base(linie)
        {
            char[] c = { ' ' };
            string[] atomi = linie.Split(c,StringSplitOptions.RemoveEmptyEntries);
            this.rata_comision = double.Parse(atomi[4]);
            this.venit_produs = double.Parse(atomi[5]);
            this.castiguri = this.Calcul_Castiguri();
        }
        public override double Calcul_Castiguri()
        {
            double x = (this.Calcul_spor_vechime(ani_vechime) + rata_comision * venit_produs);
            return x;
        }
        public override double Spor_Vechime()
        {
            return 0;
        }
        public double Rata_comision
        {
            get { return this.rata_comision; }
        }
        public double Venit_produs
        {
            get { return this.venit_produs; }
        }
        public override string ToString()
        {
            return base.ToString() + "Rata Comision : " + this.Rata_comision + Environment.NewLine + "Venit Produs : " + this.Venit_produs + Environment.NewLine;
        }
    }
}
