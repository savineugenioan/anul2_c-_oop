﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    abstract class Angajat
    {
        protected string nume, prenume;
        protected int ani_vechime;
        protected double castiguri;
        protected double spor_vechime;

        public Angajat(string nume, string prenume, int ani_vechime)
        {
            this.nume = nume;
            this.prenume = prenume;
            this.ani_vechime = ani_vechime;
            this.spor_vechime = this.Spor_Vechime();
            this.castiguri = this.Calcul_Castiguri();
        }
        public Angajat(string linie)
        {
            char[] c = { ' ' };
            string[] atomi = linie.Split(c,StringSplitOptions.RemoveEmptyEntries);
            this.nume = atomi[1];
            this.prenume = atomi[2];
            this.ani_vechime = int.Parse(atomi[3]);
            this.spor_vechime = this.Spor_Vechime();
            //this.castiguri = this.Calcul_Castiguri();
        }
        protected double Calcul_spor_vechime(int ani_vechime)
        {
            if (ani_vechime < 5)
                spor_vechime *= 0.05 ;
            else if (ani_vechime < 10)
                spor_vechime *= 0.1;
            else if (ani_vechime < 15)
                spor_vechime *= 0.15;
            else
                spor_vechime *= 0.2;
            return spor_vechime;
        }
        public abstract double Calcul_Castiguri();
        public abstract double Spor_Vechime();
        public string Nume
        {
            get {return this.nume;}
        }
        public string Prenume
        {
            get { return this.prenume; }
        }
        public int Vechime
        {
            get { return this.ani_vechime; }
        }
        public double Castiguri
        {
            get { return this.castiguri; }
        }
        public override string ToString()
        {
            return "Nume : " + this.Nume + Environment.NewLine + "Prenume : " + this.Prenume + Environment.NewLine + "Vechime : " + this.Vechime + Environment.NewLine + "Spor Vechime : " + this.spor_vechime + Environment.NewLine + "Castiguri : " + this.Castiguri + Environment.NewLine;
        }
    }
}
