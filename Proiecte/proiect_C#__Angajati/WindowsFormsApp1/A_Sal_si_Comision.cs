﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class A_Sal_si_Comision : A_Comision
    {
        //double rata_comision, venit_produs
        double salariu;
        public A_Sal_si_Comision(string nume, string prenume, int ani_vechime,double salariu, double rata_comision, double venit_produs) : base(nume, prenume, ani_vechime,rata_comision,venit_produs)
        {
            this.salariu = salariu;
            this.castiguri = this.Calcul_Castiguri();
        }
        public A_Sal_si_Comision(string linie) : base(linie)
        {
            char[] c = { ' ' };
            string[] atomi = linie.Split(c,StringSplitOptions.RemoveEmptyEntries);
            this.salariu = double.Parse(atomi[6]);
            this.rata_comision = double.Parse(atomi[4]);
            this.venit_produs = double.Parse(atomi[5]);
            this.castiguri = this.Calcul_Castiguri();
        }
        public override double Calcul_Castiguri()
        {
            double x = (this.spor_vechime * salariu + rata_comision * venit_produs);
            return x;
        }
        public override double Spor_Vechime()
        {
            return 1;
        }
        public double Salariu
        {
            get { return this.salariu; }
        }
        public override string ToString()
        {
            return base.ToString() + "Salariu : " + this.Salariu + Environment.NewLine;
        }
    }
}
