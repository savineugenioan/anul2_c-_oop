﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form_adaugare : Form
    {
        public Form_adaugare()
        {
            InitializeComponent();
        }

        string nume_fisier = "date.txt";

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label4.Visible = true;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            textBox4.Text = "";textBox4.Visible = true;
            textBox5.Text = ""; textBox5.Visible = false;
            textBox6.Text = ""; textBox6.Visible = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label4.Visible = false;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            textBox4.Text = ""; textBox4.Visible = false;
            textBox5.Text = ""; textBox5.Visible = true;
            textBox6.Text = ""; textBox6.Visible = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            textBox4.Text = ""; textBox4.Visible = true;
            textBox5.Text = ""; textBox5.Visible = true;
            textBox6.Text = ""; textBox6.Visible = true;
        }

        private void Form_adaugare_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form f = new Form1();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] chars = { ' ', '"', ',', '.', '/', '?', '!', '\\', '/', '!' };
            if (verificare() == false)
            {
                MessageBox.Show("Va rugam sa completati toate campurile corect!");
                return;
            }

            if (radioButton1.Checked == true)
                File.AppendAllText(nume_fisier,"S "+ textBox1.Text + " " + textBox2.Text + " " + textBox3.Text + " " + textBox4.Text + " " + Environment.NewLine);
            else if (radioButton2.Checked == true)
                File.AppendAllText(nume_fisier,"C "+ textBox1.Text + " " + textBox2.Text + " " + textBox3.Text + " " + textBox5.Text + " " + textBox6.Text + Environment.NewLine);
            else if (radioButton3.Checked == true)
                File.AppendAllText(nume_fisier,"SC "+ textBox1.Text + " " + textBox2.Text + " " + textBox3.Text + " " + textBox5.Text + " " + textBox6.Text + " " + textBox4.Text + Environment.NewLine);
            MessageBox.Show("Angajatul a fost adaugat cu success !");
            Form f = new Form1();
            this.Hide();
            f.Show();

            bool verificare()
            {
                int valuare;double value;
                if (textBox1.Text.Trim(chars) == string.Empty || textBox2.Text.Trim(chars) == string.Empty || textBox3.Text.Trim(chars) == string.Empty)
                    return false;
                if (!int.TryParse(textBox3.Text, out valuare))
                    return false;
                if (radioButton1.Checked == true && (textBox4.Text.Trim(chars) == string.Empty || !double.TryParse(textBox4.Text, out value)))
                    return false;
                if (radioButton2.Checked == true && (textBox5.Text.Trim(chars) == string.Empty || !double.TryParse(textBox5.Text, out value)) && (textBox6.Text.Trim(chars) == string.Empty || !double.TryParse(textBox6.Text, out value)))
                    return false;
                if (radioButton3.Checked == true && (textBox5.Text.Trim(chars) == string.Empty || !double.TryParse(textBox5.Text, out value)) && (textBox6.Text.Trim(chars) == string.Empty || !double.TryParse(textBox6.Text, out value))  && (textBox4.Text.Trim(chars) == string.Empty || !double.TryParse(textBox4.Text, out value)))
                    return false;
                return true;
            }
        }

        private void Form_adaugare_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
        }
    }
}
