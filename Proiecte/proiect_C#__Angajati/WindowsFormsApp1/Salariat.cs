﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Salariat : Angajat
    {
        double salariu;
        public Salariat(string nume, string prenume, int ani_vechime,double salariu) : base(nume, prenume, ani_vechime)
        {
            this.salariu = salariu;
            this.castiguri = this.Calcul_Castiguri();
        }
        public Salariat(string linie) : base(linie)
        {
            char[] c = { ' ' };
            string[] atomi = linie.Split(c,StringSplitOptions.RemoveEmptyEntries);
            this.salariu = double.Parse(atomi[4]);
            this.castiguri = this.Calcul_Castiguri();
        }
        public override double Calcul_Castiguri()
        {
            double x= (this.Calcul_spor_vechime(ani_vechime)*salariu + salariu);
            return x;
        }
        public override double Spor_Vechime()
        {
            return 1;
        }
        public double Salariu
        {
            get { return this.salariu; }
        }
        public override string ToString()
        {
            return base.ToString() + "Salariu : " + this.Salariu + Environment.NewLine;
        }
    }
}
