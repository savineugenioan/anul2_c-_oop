﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Angajat> angajati = new List<Angajat>();
        int nr_angajat = 0;
        string nume_fisier = "date.txt";

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Enabled = true;
            string[] x = File.ReadAllLines(nume_fisier);
            foreach (string y in x)
            {
                char[] chars = { ' ' };
                string[] atomi =y.Split(chars, StringSplitOptions.RemoveEmptyEntries);
                if (atomi[0] == "S")
                    angajati.Add(new Salariat(y));
                else if (atomi[0] == "C")
                    angajati.Add(new A_Comision(y));
                else if (atomi[0] == "SC")
                    angajati.Add(new A_Sal_si_Comision(y));
            }
            AfisareAngajat();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (angajati.Count > 1)
            {
                nr_angajat++;
                if (nr_angajat == angajati.Count - 1)
                    button2.Enabled = false;
                button1.Enabled = true;
                AfisareAngajat();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (angajati.Count > 1)
            {
                nr_angajat--;
                if (nr_angajat == 0)
                    button1.Enabled = false;
                button2.Enabled = true;
                AfisareAngajat();
            }
        }
        public void AfisareAngajat()
        {
            if (angajati.Count > 0)
            {
                textBox1.Text = angajati[nr_angajat].Nume;
                textBox2.Text = angajati[nr_angajat].Prenume;
                textBox3.Text = angajati[nr_angajat].Vechime.ToString();
                textBox4.Text = angajati[nr_angajat].Castiguri.ToString();
                string s="";
                s = angajati[nr_angajat].GetType().Name.ToString();
                switch (s)
                {
                    case ("A_Comision"): s = "Angajat pe Comision";break;
                    case ("Salariat"): s = "Angajat pe Salariu"; break;
                    case ("A_Sal_si_Comision"): s = "Angajat pe Salariu si pe Comision"; break;
                }
                textBox5.Text = s;
                if (angajati.Count == 1)
                {
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new Form_adaugare();
            f.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string s = "";
            s = angajati[nr_angajat].GetType().Name.ToString();
            A_Comision x; Salariat y; A_Sal_si_Comision z;
            switch (s)
            {
                case ("A_Comision"): x = (A_Comision)angajati[nr_angajat];s = x.ToString(); break;
                case ("Salariat"): y = (Salariat)angajati[nr_angajat]; s = y.ToString(); break;
                case ("A_Sal_si_Comision"): z = (A_Sal_si_Comision)angajati[nr_angajat]; s = z.ToString(); break;
            }
            Globalz.s = s;
            Form f = new Detalii_Angajat();
            f.ShowDialog();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Enabled=true;
        }
    }
}
