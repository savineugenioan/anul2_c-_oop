﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiune_Studenti
{
    class Student
    {
        string nume;
        string grupa;
        int nota_economie, nota_mate, nota_info;
        public string Nume
            {
                get { return this.nume; }
                set { this.nume = value; }
            }
        public string Grupa
        {
            get { return this.grupa; }
            set { this.grupa = value; }
        }
        public int Nota_economie
        {
            get { return this.nota_economie; }
            set
            {
                if (value > 0 && value < 11)
                    this.nota_economie = value;
                else
                    this.nota_economie = 0;
            }
        }
        public int Nota_Mate
        {
            get { return this.nota_mate; }
            set
            {
                if (value > 0 && value < 11)
                    this.nota_mate = value;
                else
                    this.nota_mate = 0;
            }
        }
        public int Nota_Info
        {
            get { return this.nota_info; }
            set
            {
                if (value > 0 && value < 11)
                    this.nota_info = value;
                else
                    this.nota_info = 0;
            }
        }
        public Student(string nume,string grupa,int economie,int mate,int info)
        {
            contor++;
            this.nume = nume;
            this.grupa = grupa;
            this.nota_economie = economie;
            this.nota_mate = mate;
            this.nota_info = info;
            if (this.nota_economie < 1 || this.nota_economie > 10)
                this.nota_economie = 0;
            if (this.nota_mate < 1 || this.nota_mate > 10)
                this.nota_mate = 0;
            if (this.nota_info < 1 || this.nota_info > 10)
                this.nota_info = 0;
        }
        public override string ToString()
        {
            return "Studentul "+nume+" este in grupa "+grupa+" si are nota "+nota_economie+" la economie, "+Nota_Mate +"la mate"+nota_info+"la info";
        }
        public Student(string rand)
        {
            char[] c = { ' ' };
            string[] atomi = rand.Split(c,StringSplitOptions.RemoveEmptyEntries);
            this.nume = atomi[0];
            this.grupa = atomi[1];
            this.nota_economie = int.Parse(atomi[2]);
            this.nota_mate = int.Parse(atomi[3]);
            this.nota_info = int.Parse(atomi[4]);
            if (this.nota_economie < 1 || this.nota_economie > 10)
                this.nota_economie = 0;
            if (this.nota_mate < 1 || this.nota_mate > 10)
                this.nota_mate = 0;
            if (this.nota_info < 1 || this.nota_info > 10)
                this.nota_info = 0;
            contor++;
        }
        public static int contor = 0;
        
    }
}
