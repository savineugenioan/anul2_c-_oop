﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace Gestiune_Studenti
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string nume_fisier = "note.txt";
        int nr_student = 0;
        List<Student> studenti = new List<Student>();
    
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] x = File.ReadAllLines(nume_fisier);
            foreach(string y in x)
            {
                //char[] chars = { ' ' };
                //string[] atomi =y.Split(chars, StringSplitOptions.RemoveEmptyEntries);
                studenti.Add(new Student(y));
            }
            AfisareStudent();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new Form_adaugare();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (studenti.Count > 1)
            {
                nr_student++;
                if (nr_student == studenti.Count - 1)
                    button2.Enabled = false;
                button1.Enabled = true;
                label2.Text = (nr_student + 1) + "";
                AfisareStudent();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (studenti.Count > 1)
            {
                nr_student--;
                if (nr_student == 0)
                    button1.Enabled = false;
                button2.Enabled = true;
                label2.Text = (nr_student + 1) + "";
                AfisareStudent();
            }
        }

        public void AfisareStudent()
        {       if (studenti.Count > 0)
            {
                textBox1.Text = studenti[nr_student].Nume;
                textBox2.Text = studenti[nr_student].Grupa;
                textBox3.Text = studenti[nr_student].Nota_economie.ToString();
                textBox4.Text = studenti[nr_student].Nota_Mate.ToString();
                textBox5.Text = studenti[nr_student].Nota_Info.ToString();
                if(studenti.Count ==1)
                {
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
                if (int.Parse(textBox3.Text) == 0)
                    textBox3.Text = "( Absent )";
                if (int.Parse(textBox4.Text) == 0)
                    textBox4.Text = "( Absent )";
                if (int.Parse(textBox5.Text) == 0)
                    textBox5.Text = "( Absent )";
            }     
        else
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
