﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gestiune_Studenti
{
    public partial class Form_adaugare : Form
    {
        string nume_fisier = "note.txt";
        public Form_adaugare()
        {
            InitializeComponent();
        }

        private void Form_adaugare_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form f = new Form1();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] chars = { ' ', '"', ',', '.', '/','?','!','\\','/','!'};
            if (verificare()==false)
            {
                MessageBox.Show("Va rugam sa completati toate campurile corect!");
                return;
            }

            File.AppendAllText(nume_fisier, textBox1.Text + " " + textBox2.Text + " " + textBox3.Text + " " + textBox4.Text + " " + textBox5.Text + Environment.NewLine);
            MessageBox.Show("Studentul a fost adaugat cu success !");
            Form f = new Form1();
            this.Hide();
            f.Show();
            bool verificare()
            {
                int valuare,value,valuee;
                if (textBox1.Text.Trim(chars) == string.Empty || textBox2.Text.Trim(chars) == string.Empty || textBox3.Text.Trim(chars) == string.Empty || textBox4.Text.Trim(chars) == string.Empty || textBox5.Text.Trim(chars) == string.Empty ||
                    !int.TryParse(textBox3.Text,out valuare) || !int.TryParse(textBox4.Text, out value) || !int.TryParse(textBox5.Text, out valuee))
                    return false;
                return true;
            }
        }

        private void Form_adaugare_Load(object sender, EventArgs e)
        {

        }
    }
}
